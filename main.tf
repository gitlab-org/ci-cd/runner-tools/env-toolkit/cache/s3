variable "namespace" {
  type = string
}

variable "aws-profile" {
  type = string
}

provider "aws" {
  region  = "eu-west-1"
  profile = var.aws-profile
}

resource "aws_s3_bucket" "cache-bucket" {
  bucket = "${var.namespace}-runner"
  acl    = "private"

  tags = {
    Name        = "${var.namespace}-runner"
    Environment = "Dev"
    Owner       = var.namespace
    Group       = "runner"
  }
}

