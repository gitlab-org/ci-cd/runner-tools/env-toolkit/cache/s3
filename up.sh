#!/usr/bin/env bash

NAMESPACE="$1"
AWS_PROFILE="${2:-default}"

terraform init

terraform apply -var "namespace=${NAMESPACE}" -var "aws-profile=${AWS_PROFILE}"

ACCESS_KEY=$(grep -A 2 "$AWS_PROFILE" ~/.aws/credentials | grep 'aws_access_key_id' | cut -b 21-50)
SECRET_ACCESS_KEY=$(grep -A 2 "$AWS_PROFILE" ~/.aws/credentials | grep 'aws_secret_access_key' | cut -b 25-80)

echo "Add the following to your config.toml file"
cat << EOF
  [runners.cache]
    Type = "s3"
    Path = "cache"
    Shared = true
    [runners.cache.s3]
      ServerAddress = "s3.amazonaws.com"
      BucketName = "${NAMESPACE}-runner"
      BucketLocation = "eu-west-1"
      Insecure = false
      AccessKey = "${ACCESS_KEY}"
      SecretKey = "${SECRET_ACCESS_KEY}"
EOF
