#!/usr/bin/env bash

NAMESPACE="$1"
AWS_PROFILE="${2:-default}"

terraform destroy -var "namespace=${NAMESPACE}" -var "aws-profile=${AWS_PROFILE}"
