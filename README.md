# Set up S3 cache

Create infrastructure and set up configuration for
[`[runners.cache.s3]`](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runnerscaches3-section).

## Prerequisites

- [Terraform installed](https://www.terraform.io/downloads.html)
- [aws
  cli](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html)
  installed and configued.

## Run

- $NAMESPACE: Just a namespace to prefix the resources created.
- $AWS_PROFILE: If your aws cli has multiple [named
  profiles](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html),
  if not specified `default` is used.

```shell
# up.sh $NAMESPACE $AWS_PROFILE
./up.sh steveazz dev
```

## Cleanup

- $NAMESPACE: Just a namespace to prefix the resources created.
- $AWS_PROFILE: If your aws cli has multiple [named
  profiles](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html),
  if not specified `default` is used.

```shell
# down.sh $NAMESPACE $AWS_PROFILE
./down.sh steveazz dev
```
